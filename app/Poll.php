<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Poll extends Model
{
    protected $fillable = ['poll_description'];
    public $timestamps = false;
    protected $hidden = ['id', 'views', 'options'];
    //protected $appends = ['poll_id'];
    //protected $relations = ['poll_options'];


    public function options()
    {
        return $this->hasMany('App\PollOption');//->select(['poll_id', 'id', 'option_description']);
    }

    public function getPollIdAttribute()  //acessor (buscar) //mutator
    {
        return $this->attributes['poll_id'] = $this->id;
    }
}
