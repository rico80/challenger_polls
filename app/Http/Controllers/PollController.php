<?php

namespace App\Http\Controllers;

use App\Poll;
use App\PollOption;
use Illuminate\Http\Request;

class PollController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function index()
    {
        $polls = Poll::with('options')->get();
        return response()->json($polls, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $poll = Poll::create($data);
        $dataOption['poll_id'] = $poll->id;
        foreach ($data['options'] as $option) {
            $dataOption['option_description'] = $option;
            PollOption::create($dataOption);
        }


        return response()->json(['option_id' => $poll->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function show($id)
    {
        $poll = Poll::with('options')->find($id);
        $poll->views++;
        $poll->save();

        $poll->options = $poll->options->makeHidden('qty');

        //return response()->json($poll);

        return [
            'poll_id' => $poll->id,
            'poll_description' => $poll->poll_description,
            'options' => $poll->options,
        ];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function vote(Request $request, int $pollId)
    {
        $data = $request->all();
        $poll = Poll::find($pollId);

        if (is_null($poll)) {
            return response()->json(['error' => 'Not found'], 404);
        }

        $pollOption = PollOption::find($data['option_id']);
        $pollOption->votes++;
        $pollOption->save();
        return response()->json(['option_id' => $pollOption->id]);
    }

    public function stats(Request $request, int $pollId)
    {
        $poll = Poll::with('options')->find($pollId);
        $poll = $poll->makeVisible('views');
        $poll = $poll->makeHidden('poll_description');
        //$poll->options = $poll->options->makeVisible('votes');
        //$poll->options = $poll->options->makeHidden('option_description');
        $poll->votes = $poll->options->makeHidden('option_description', 'options');

        //return response()->json($poll);

        return [
            'poll' => $poll,
        ];
    }
}
