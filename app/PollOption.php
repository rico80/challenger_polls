<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PollOption extends Model
{
    protected $fillable = ['poll_id', 'option_description'];
    public $timestamps = false;
    protected $hidden = ['id', 'poll_id', 'votes', 'options'];
    protected $appends = ['option_id', 'qty'];


    public function poll()
    {
        return $this->belongsTo('App\Poll');
    }

    public function getOptionIdAttribute()
    {
        return $this->attributes['option_id'] = $this->id;
    }

    public function getQtyAttribute(){
        return $this->attributes['qty'] = $this->votes;
    }
}
